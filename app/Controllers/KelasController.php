<?php namespace App\Controllers;

use App\Models\KelasModel;
use App\Models\SiswakelasModel;

class KelasController extends BaseController
{	
	protected $kelasModel;
	protected $siswakelasModel;

	// inisialisasi model pada method construct
	public function __construct()
	{
		$this->kelasModel = new KelasModel();
		$this->siswakelasModel = new SiswakelasModel();
	}
		
	public function index()
	{
		// mendapatkan keyword dari searchbar
		$keyword = $this->request->getVar('key') ?? "";

		// pemfilteran data jika keyword tidak kosong
		if($keyword !== ""){
			$this->kelasModel->like('nama_kelas',strtolower($keyword))
							 ->orLike('nama_kelas', strtoupper($keyword))
							 ->orLike('nama_kelas', $keyword);					
		}

		// iinsialisasi data yang akan di-pass ke view index.		
		// jika data didapat dari query builder, maka harus diurut berdasarkan
		// logika proses query builder yang digunakan untuk menghindari error

		$content = [
			'keyword' 	=> $keyword,			
			'count' 	=> $this->kelasModel->countAllResults(false),
			'kelas' 	=> $this->kelasModel->paginate(12),
			'pager' 	=> $this->kelasModel->pager,
		];
		
		// insisialisasi data yang akan dipass view template,		
		// hasil return view index dipassing juga ke dalam view dashboard

		$data = [
			'title'		=> 'Kelas',
			'nav_id' 	=> 2,
			'content' 	=> view('pages/kelas/index', $content)
		];

		// mengembalikan nilai fungsi dashboard
		return view('templates/dashboard', $data);
	}

	public function detail($id)
	{
		// memastikan terlebih dahulu jika kelas yang dicari ditemukan
		if ( ! $kelas = $this->kelasModel->find($id))
		{
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		// mendapatkan value page dari query string
		$page = max(1, intval($this->request->getVar('page')));

		// mempersiapkan query builder		
		$this->siswakelasModel->select('siswa.id_siswa, nama_siswa, jenisk_siswa, alamat_siswa')
							  ->join('siswa', 'siswa.id_siswa = siswa_kelas.id_siswa', 'inner')
							  ->where('siswa_kelas.id_kelas', $id)
							  ->getCompiledSelect(FALSE);
		
		// iinsialisasi data yang akan di-pass ke view index,		
		// jika data didapat dari query builder, maka harus diurut berdasarkan
		// logika proses query builder yang digunakan untuk menghindari error

		$content = [
			'kelas' 		=> $kelas,
			'max_count'		=> $this->siswakelasModel->countAll(FALSE),
			'count' 		=> $this->siswakelasModel->countAllResults(FALSE),
			'siswa' 		=> $this->siswakelasModel->get()->getResultArray(),
		];
		
		// insisialisasi data yang akan dipass view template,		
		// hasil return view index dipassing juga ke dalam view dashboard

		$data = [
			'title' 	=> $kelas['nama_kelas'],
			'nav_id' 	=> 2,
			'content' 	=> view('pages/kelas/detail', $content)
		];

		// mengembalikan nilai fungsi dashboard
		return view('templates/dashboard', $data);
	}
	
	public function kelas($id)
    {	
        helper('mpdf');
		$html = '<div style="text-align:center"><h1>CodeIgniter4 + mPDF</h1><h4>We are good to go, awesome!</h4></div><ul>';
		for($i = 1; $i <= 10; $i++)
		{
			$html .= '<li>Reports here......</li>';
		}
		$html .= '</ul>';
        echo_mpdf($html, "kelas-$id", $this->response, 'Legal-L');
    }
	//--------------------------------------------------------------------
	
}
