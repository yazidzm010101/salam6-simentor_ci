<?php namespace App\Controllers;

class HomeController extends BaseController
{
	public function index()
	{
		$home = [
			'user' => 'Yazid Zaidan Mujadid'
		];
		$data = [
			"content" => view('pages/home/index.php', $home),
			"nav_id" => 1
		];
		return view('templates/dashboard', $data);		
	}

	//--------------------------------------------------------------------

}
