<?php namespace App\Models;

use CodeIgniter\Model;

class KelasModel extends Model
{
    protected $table = 'kelas';
    protected $primaryKey = 'id_kelas';
    protected $returnType  = 'array';
    protected $allowedFields = ['nama_kelas', 'status_kelas'];
}