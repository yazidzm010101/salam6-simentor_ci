<?php namespace App\Models;

use CodeIgniter\Model;

class SiswakelasModel extends Model
{
    protected $table = 'siswa_kelas';
    protected $primaryKey = 'id_siswa_kelas';
    protected $returnType  = 'array';
    protected $allowedFields = ['id_siswa', 'id_kelas', 'id_tahunajaran'];
}