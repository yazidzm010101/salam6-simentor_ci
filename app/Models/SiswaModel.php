<?php namespace App\Models;

use CodeIgniter\Model;

class SiswaModel extends Model
{
    protected $table = 'siswa';
    protected $primaryKey = 'id_siswa';
    protected $returnType  = 'array';
    protected $allowedFields = ['nama_siswa', 'jenisk_siswa', 'alamat_siswa'];
}