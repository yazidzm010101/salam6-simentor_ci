<?php

use \Mpdf\Mpdf;

function echo_mpdf($html, $filename, $response, $format='A4' )
{
    $mpdf = new Mpdf(['mode' => 'utf-8', 'format' => $format]);
    $mpdf->SetDisplayMode('fullpage');
    $mpdf->SetTitle($filename);
    $mpdf->WriteHTML($html);
    
    $response->setHeader('Content-type', 'application/pdf')
             ->setHeader('Content-Disposition', "inline; filename=$filename")
             ->setHeader('Content-Transfer-Encoding', 'binary')
             ->setHeader('Accept-Ranges', 'bytes')             
             ->setBody($mpdf->Output());             
}