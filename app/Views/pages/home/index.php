<?php
    // Mendapatkan nama pementor, kemudian string dipecah dengan delimiter whitespace
    // sehingga terbentuk array kata. Kemudian diambil inisial dua kata pertama, jika
    // undefined atau error nilai yang diambil adalah NULL, sehingga berlaku untuk
    // nama yang terdiri dari satu kata
    $user = $user ?? 'Pementor';
    $user_split = preg_split('/\s+/', $user);
    $initial_first = $user_split[0][0] ?? NULL;
    $initial_last = $user_split[1][0] ?? NULL;
?>
<div class="section">

    <div class="center-align">
        <span class="circle-user amber darken-2">
            <div class="white-text"><?php echo $initial_first.$initial_last ?></div>
        </span>        
        <h5><?php echo "Selamat datang, {$user}" ?></h5>
        <p>Kelola data siswa, kelas, serta kehadiran untuk membuat Simentor bekerja lebih baik untukmu.</p>
    </div>

    <div class="row row-flex">
        <div class="col s12 m6">
            <?php echo
                view('templates/card', $data = [
                        'card' => [
                            'Input kehadiran', 
                            'Melakukan pencatatan rekaman kehadiran mentoring pada setiap pertemuan',
                            base_url('assets/img/card-clipboard.png'),
                            'input-kehadiran',
                            'Buka daftar seluruh kelas' ,
                            base_url('kelas')
                        ]
                    ]
                )
            ?>
        </div>

        <div class="col s12 m6">
            <?php echo
                view('templates/card',$data = [
                        'card' => [
                            'Cetak kehadiran', 
                            'Simpan secara lokal rekaman kehadiran dalam dokumen <em>pdf</em> atau <em>spreadsheet</em>',
                            base_url('assets/img/card-printer.png'),
                            'input-kehadiran', 
                            'Cetak kehadiran sekarang' ,
                            base_url('kelas/cetak')
                        ]
                    ]
                ) 
            ?>
        </div>
    </div>    
    </div>
</div>