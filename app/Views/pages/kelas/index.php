<section class="section">

    <!-- HEADER KONTEN -->
    <div class="center-align">
        <h5>
            Daftar Kelas
        </h5>
        <p>
            Berikut ini adalah daftar kelas aktif pada tahun ajaran 2019/2020
        </p>
    </div>

    <!-- FORM PENCARIAN KELAS -->
    <form method="GET">
        <div class="input-field">
            <input placeholder="Pencarian" id="key" name="key" type="text" class="validate"
                value="<?php echo $keyword ?? NULL ?>">
            <button class="btn-flat btn-floating waves-effect waves-dark white" type="submit">
                <i class="material-icons grey-text">search</i>
            </button>
        </div>
    </form>

    <!-- PENGHITUNG JUMLAH DATA -->
    <p class="grey-text">
        <?php echo "Menampilkan " . count($kelas) . " dari {$count} entri"?>
    </p>

    <!-- TABEL KELAS -->
    <div class="row">            
            <?php foreach($kelas as $item):?>
                <div class="col s12 m6 l4 xl3">
                    <a href="<?php echo base_url(['kelas', $item['id_kelas']]) ?>">
                        <div class="card with-border hoverable grey-text text-darken-2" style="padding: 1.5rem; margin-bottom: 0.25rem;">
                            <span class="card-title" style="letter-spacing: 2px; font-weight: 400;">
                                <?php echo $item['nama_kelas'] ?>
                            </span>
                        </div>
                    </a>
                </div>
            <?php endforeach;?>        
    </div>

</section>


<!-- PAGINASI HALAMAN -->
<section class="section center-align">
    <?php echo $pager->links() ?>         
</section>
