<section class="section">

    <!-- HEADER KONTEN -->
    <div class="center-align">
        <h5>
            <?php echo $kelas['nama_kelas'] ?>
        </h5>
        <p>
            Berikut ini adalah daftar siswa kelas <?php echo $kelas['nama_kelas'] ?>
        </p>
    </div>

</section>


<form id="live-search" autocomplete="off">
    <div class="row">
        <div class="col s12 m6 l4 offset-m6 offset-l8">
            <div class="input-field">
                <input placeholder="Pencarian" name="keyword" type="text" class="validate keyword" style="margin-bottom: 0;">
                <button class="btn-flat btn-floating waves-effect waves-dark white" type="submit">
                    <i class="material-icons grey-text">search</i>
                </button>
            </div>
        </div>
    </div>
</form>

<!-- TABEL KELAS -->
<section class="section with-border" style="margin-bottom: 3rem; padding:unset">
    <div class="overflow-wrapper" style="height: 70vh;">

        <table class="table-links highlight" id="siswa-table">
            <thead>
                <tr>
                    <th style="min-width: 3rem;">
                        <span class="th-text">#</span>
                        <i class="material-icons">arrow_drop_down</i>
                    </th>
                    <th>
                        <span class="th-text">Nama</span>
                    </th>
                    <th>
                        <span class="th-text">Jenis Kelamin</span>
                    </th>
                    <th>
                        <span class="th-text">Alamat</span>
                    </th>
                </tr>
            </thead>
            <tbody>

                <!-- SETIAP ANGGOTA DALAM DATA DI MAP KE DALAM TABEL -->
                <?php foreach ($siswa as $id => $item) : ?>
                    <tr onclick="window.location = `<?php echo base_url(['siswa', $item['id_siswa']]) ?>`">
                        <td>
                            <?php echo $id + 1 ?>
                        </td>
                        <td>
                            <?php echo $item['nama_siswa'] ?>
                        </td>
                        <td>
                            <?php echo $item['jenisk_siswa'] === 'L' ?
                                'Ikhwan' :
                                'Akhwat' ?>
                        </td>
                        <td>
                            <?php echo $item['alamat_siswa'] ?>
                        </td>
                    </tr>
                <?php endforeach; ?>

            </tbody>
        </table>
    </div>

</section>


<div class="fixed-action-btn">
    <a class="btn-floating btn-large waves-effect waves-light amber darken-3">
        <i class="large material-icons">more_vert</i>
    </a>
    <ul>
        <li><a href="<?php echo base_url("kelas/{$id}/cetak") ?>" class="btn-floating btn-large waves-effect waves-light red"><i class="fas fa-file-pdf"></i></a></li>
        <li><a class="btn-floating btn-large waves-effect waves-light green"><i class="fas fa-file-excel"></i></a></li>
        <li><a class="btn-floating btn-large waves-effect waves-light blue"><i class="fas fa-clipboard-list"></i></a></li>
    </ul>
</div>