<div class="header white">
    <div class="row valign-wrapper">
        <div class="col s6 m7">
            <img src="<?= base_url('assets/img/logo_l_32.png') ?>" alt="Simentor" class="responsive-img">
        </div>
        <div class="col s6 m5 right-align">
            <a href="#!" class="btn-flat btn-floating waves-effect waves-dark white">
                <i class="material-icons grey-text">more_vert</i>
            </a>
            <a href="#!" class="btn-flat btn-floating waves-effect waves-dark center-align amber darken-2 white-text">
                Y
            </a>
        </div>
    </div>
    <div class="navigation row">
        <div class="col s12">
            <ul class="tabs">
                <li class="tab"><a target="_self" href="/">Beranda</a></li>
                <li class="tab"><a target="_self" href="/kelas" class="active">Kelas</a></li>                
            </ul>        
        </div>
    </div>
</div>