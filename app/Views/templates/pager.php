<?php $pager->setSurroundCount(2) ?>

<ul class="pagination">
<?php if ($pager->hasPrevious()) : ?>    
    <li>
        <a href="<?= $pager->getPrevious() ?>" aria-label="<?= lang('Pager.previous') ?>">
            <span aria-hidden="true"><i class="material-icons">chevron_left</i></span>
        </a>
    </li>
<?php else: ?>
    <li class="disabled">
        <a href="#!">
            <span aria-hidden="true"><i class="material-icons">chevron_left</i></span>
        </a>
    </li>
<?php endif ?>

<?php foreach ($pager->links() as $link) : ?>
    <li <?= $link['active'] ? 'class="active"' : '' ?>>
        <a href="<?= $link['uri'] ?>">
            <?= $link['title'] ?>
        </a>
    </li>
<?php endforeach ?>



<?php if ($pager->hasNext()) : ?>
    <li>
        <a href="<?= $pager->getNext() ?>" aria-label="<?= lang('Pager.next') ?>">
            <span aria-hidden="true"><i class="material-icons">chevron_right</i></span>
        </a>
    </li>    
<?php else: ?>
    <li class="disabled">
        <a href="#!">
            <span aria-hidden="true"><i class="material-icons">chevron_right</i></span>
        </a>
    </li>
<?php endif;?>    
</ul>