<?php
$title = $title ?? 'Beranda';
$nav_id = $nav_id ?? -1;
$content = $content ?? '';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php echo view('templates/header') ?>
    <link rel="stylesheet" href="<?php echo base_url('assets/css/index.min.css') ?>" media="print" onload="this.media='all'">
    <title><?php echo "{$title} - SIMENTOR"?></title>
</head>
<body>
    <?php echo view('templates/navigation') ?>    
    <main>
        <div class="container">
            <?php echo $content ?>
        </div>
    </main>
    <?php echo view('templates/footer') ?>
    <script>
        var scr = document.getElementById('scr');
        scr.addEventListener("load", function()
        {
            CUSTOM.navtablink(".navigation",<?php echo $nav_id ?>);
            CUSTOM.navscroll(".header");
            CUSTOM.livesearch("#live-search","#siswa-table");
            M.AutoInit();
            var elems = document.querySelectorAll('.fixed-action-btn');
            var instances = M.FloatingActionButton.init(elems, {                
                hoverEnabled: false
            });
        })        
    </script>
</body>
</html>