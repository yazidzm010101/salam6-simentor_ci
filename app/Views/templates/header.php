<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- stylesheet -->
<link rel="stylesheet" href="<?= base_url('assets/css/googlefont.css') ?>" media="print" onload="this.media='all'">
<link rel="stylesheet" href="<?= base_url('assets/css/materialize.min.css') ?>" media="print" onload="this.media='all'">
<!-- javascript -->
<script async src="https://kit.fontawesome.com/bcf655051e.js" crossorigin="anonymous"></script>
<script defer type="text/javascript" src="<?= base_url('assets/js/jquery.min.js') ?>"></script>
<script defer type="text/javascript" src="<?= base_url('assets/js/materialize.min.js') ?>"></script>
<script defer id="scr" type="text/javascript" src="<?= base_url('assets/js/index.js') ?>"></script>