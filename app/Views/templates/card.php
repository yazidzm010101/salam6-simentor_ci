<?php
$cardTitle = $card[0] ?? 'Judul card';
$cardContent = $card[1] ?? 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Natus architecto adipisci eligendi minus officia enim obcaecati.';
$cardImg = $card[2] ?? 'https://dummyimage.com/300.png/09f/fff';
$cardImgAlt = $card[3] ?? 'card-alt';
$cardAction = $card[4] ?? 'Adipisci eligendi minus officia enim obcaecati.';
$cardHref = $card[5] ?? '#1';
?>
<div class="card with-border hoverable">
    <div class="card-content">                            
        <div class="row">
            <div class="col s8 l9">
                <span class="card-title"><strong><?= $cardTitle ?></strong></span>
                <div class="grey-text text-darken-2">
                    <p><?= $cardContent ?></p>
                </div>
            </div>
            <div class="col s4 l3 center-align">
                <img class="responsive-img" src="<?= $cardImg ?>" alt="<?= $cardImgAlt ?>">
            </div>
        </div>
    </div>
    <div class="card-action">
        <a href="<?= $cardHref ?>" class="btn-flat white green-text text-darken-2 waves-effect waves-dark"><?= $cardAction ?></a>
    </div>
</div>