<footer class="page-footer footer-custom" style="padding: unset;">
    <div class="footer-copyright" style="border-top: 1px solid rgba(0,0,0,0.08);">
        <div class="container">            
            <a class="grey-text left valign-wrapper" href="#!">
                <i class="fab fa-instagram left"></i>
                <div class="left">@salam6</div>                
            </a>
            <a class="grey-text left valign-wrapper" href="#!">
                <i class="fas fa-at left"></i>
                <div class="left">salam6depok@gmail.com</div>
            </a>
            <div class="right grey-text">
                © 2020 Salam 6 Depok
            </div>
        </div>
    </div>
</footer>