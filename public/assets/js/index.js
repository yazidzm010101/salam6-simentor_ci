var CUSTOM = {
    navtablink: function(selector, activeIndex = -1)
    {
        var navlink = $(selector).find('.tabs .tab a');
        var activeIndex = Math.min(activeIndex, navlink.length);

        $(navlink).removeClass('active');
        if(activeIndex > 0)
        {
            $(navlink[activeIndex - 1]).addClass('active');
            $(selector).find('.tabs').tabs();
        }    
    },
    navscroll: function(selector)
    {
        $(window).scroll(function()
        {
            windowTop = $(window).scrollTop();

            if(windowTop > 0)
            {
                $(selector).addClass('float')
            }else
            {
                $(selector).removeClass('float')
            }
        });
    },
    tablesort: function(selector)
    {
                
    },
    livesearch: function(selector, target)
    {
        var form = $(selector);
        var textfield = $(form).find('.keyword');        
        var tablerow = $(target).find('tbody tr');    

        $(form).on('submit', function(e)
        {
            e.preventDefault();
        });
            
        $(textfield).on('change keyup paste', function(e)
        {
            var keyword = $(this).val();
            setTimeout(function()
            {
                if(! $.trim(keyword) || e.key == "Backspace"){
                    console.log(true);
                    $(target).find('.kbTnfx').css('display','table-row').removeClass('kbTnfx');
                }else{
                    $(tablerow).each(function(){
                        var data = $.trim($(this).children('td').text().toLowerCase());                                        
                        if(data.indexOf(keyword) > 0){
                            $(this).css('display','table-row');                        
                        }else{
                            $(this).addClass('kbTnfx')
                            $(this).css('display','none');
                        }
                    });
                }
            }, 250)
        })        
    }
}